declare type CoscineType = {
  i18n: {
    resourceedit: VueI18n.LocaleMessages | undefined;
  };
};

declare const coscine: CoscineType;

declare let _spPageContextInfo: any;

declare module "vue-router";
