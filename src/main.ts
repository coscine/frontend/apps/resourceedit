import jQuery from "jquery";
import BootstrapVue, { componentsPlugin } from "bootstrap-vue";
import Vue, { Component, ComponentOptions, VNode } from "vue";
import ResourceEditApp from "./ResourceEditApp.vue";
import VueI18n from "vue-i18n";
import { LanguageUtil } from "@coscine/app-util";

Vue.config.productionTip = false;
Vue.use(BootstrapVue);
Vue.use(VueI18n);

jQuery(() => {
  const i18n = new VueI18n({
    locale: LanguageUtil.getLanguage(),
    messages: coscine.i18n.resourceedit,
    silentFallbackWarn: true,
  });
  new Vue({
    render: (h) => h(ResourceEditApp),
    i18n,
  }).$mount("resourceedit");
});
